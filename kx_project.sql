-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 01, 2018 at 04:30 AM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kx_project`
--

-- --------------------------------------------------------

--
-- Table structure for table `kx_branch`
--

CREATE TABLE `kx_branch` (
  `id` int(10) NOT NULL,
  `level_id` int(10) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kx_branch`
--

INSERT INTO `kx_branch` (`id`, `level_id`, `name`) VALUES
(1, 1, 'IT'),
(2, 1, 'Mechanical');

-- --------------------------------------------------------

--
-- Table structure for table `kx_day`
--

CREATE TABLE `kx_day` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kx_faculty`
--

CREATE TABLE `kx_faculty` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `user_name` varchar(50) NOT NULL,
  `email` varchar(60) NOT NULL,
  `contact_no` varchar(10) NOT NULL,
  `field_name` varchar(50) NOT NULL,
  `level_name` varchar(50) NOT NULL,
  `branch_name` varchar(50) NOT NULL,
  `subject_name` varchar(50) NOT NULL,
  `topic_name` varchar(80) NOT NULL,
  `skill_name` varchar(50) NOT NULL,
  `day_name` varchar(50) NOT NULL,
  `hours` varchar(50) NOT NULL,
  `city_name` varchar(50) NOT NULL,
  `state_name` varchar(50) NOT NULL,
  `loc_preference` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kx_field`
--

CREATE TABLE `kx_field` (
  `id` int(10) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kx_field`
--

INSERT INTO `kx_field` (`id`, `name`) VALUES
(1, 'Engineering'),
(2, 'Pharmacy');

-- --------------------------------------------------------

--
-- Table structure for table `kx_level`
--

CREATE TABLE `kx_level` (
  `id` int(10) NOT NULL,
  `field_id` int(10) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kx_level`
--

INSERT INTO `kx_level` (`id`, `field_id`, `name`) VALUES
(1, 1, 'Degree'),
(2, 1, 'Diploma');

-- --------------------------------------------------------

--
-- Table structure for table `kx_location`
--

CREATE TABLE `kx_location` (
  `id` int(10) NOT NULL,
  `city_name` varchar(50) NOT NULL,
  `state_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kx_student`
--

CREATE TABLE `kx_student` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `field_name` varchar(50) NOT NULL,
  `level_name` varchar(50) NOT NULL,
  `branch_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kx_subject`
--

CREATE TABLE `kx_subject` (
  `id` int(10) NOT NULL,
  `branch_id` int(10) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kx_subject`
--

INSERT INTO `kx_subject` (`id`, `branch_id`, `name`) VALUES
(1, 1, 'Data Structure'),
(2, 1, 'Computer Networks');

-- --------------------------------------------------------

--
-- Table structure for table `kx_subtopic`
--

CREATE TABLE `kx_subtopic` (
  `id` int(10) NOT NULL,
  `topic_id` int(10) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kx_subtopic`
--

INSERT INTO `kx_subtopic` (`id`, `topic_id`, `name`) VALUES
(1, 1, 'SIngly Linked List'),
(2, 1, 'Doubly Linked List');

-- --------------------------------------------------------

--
-- Table structure for table `kx_time`
--

CREATE TABLE `kx_time` (
  `id` int(11) NOT NULL,
  `from_time` time(6) NOT NULL,
  `to_time` time(6) NOT NULL,
  `hours` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kx_topic`
--

CREATE TABLE `kx_topic` (
  `id` int(10) NOT NULL,
  `subject_id` int(10) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kx_topic`
--

INSERT INTO `kx_topic` (`id`, `subject_id`, `name`) VALUES
(1, 1, 'Linked List'),
(2, 1, 'Stack');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `kx_branch`
--
ALTER TABLE `kx_branch`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kx_day`
--
ALTER TABLE `kx_day`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kx_faculty`
--
ALTER TABLE `kx_faculty`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kx_field`
--
ALTER TABLE `kx_field`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kx_level`
--
ALTER TABLE `kx_level`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kx_location`
--
ALTER TABLE `kx_location`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kx_student`
--
ALTER TABLE `kx_student`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kx_subject`
--
ALTER TABLE `kx_subject`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kx_subtopic`
--
ALTER TABLE `kx_subtopic`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kx_time`
--
ALTER TABLE `kx_time`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kx_topic`
--
ALTER TABLE `kx_topic`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kx_branch`
--
ALTER TABLE `kx_branch`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `kx_field`
--
ALTER TABLE `kx_field`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `kx_level`
--
ALTER TABLE `kx_level`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `kx_subject`
--
ALTER TABLE `kx_subject`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `kx_subtopic`
--
ALTER TABLE `kx_subtopic`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `kx_topic`
--
ALTER TABLE `kx_topic`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
